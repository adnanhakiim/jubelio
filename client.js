const ampq = require('amqplib');
const config = require('./config');

const { RMQ_HOST, RMQ_PASSWORD, RMQ_PORT, RMQ_USER, PUSH_QUEUE } = require('./config');

const DEAD_LETTERS = `${PUSH_QUEUE}-dead-letters`;
const WORKER_EXCHANGE = `${PUSH_QUEUE}-exchange`;
const DL_EXCHANGE = `${PUSH_QUEUE}-dead-exchange`;

const RMQ = `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}:${RMQ_PORT}/notify`;
const open = ampq.connect(RMQ);

const service = {
  openChannel: async () => {
    const connection = await open;
    const channel = await connection.createChannel();

    await channel.assertExchange(WORKER_EXCHANGE, 'direct', { durable: false, autoDelete: false });
    await channel.assertExchange(DL_EXCHANGE, 'fanout', { durable: true, autoDelete: false });

    await channel.assertQueue(PUSH_QUEUE, {
      durable: false,
      messageTtl: 180000,
      deadLetterExchange: DL_EXCHANGE,
    });
    await channel.assertQueue(DEAD_LETTERS, { durable: true, messageTtl: 60000 });

    await channel.bindQueue(PUSH_QUEUE, WORKER_EXCHANGE, '');
    await channel.bindQueue(DEAD_LETTERS, DL_EXCHANGE);

    channel.prefetch(1);

    return { connection, channel };
  },
  getMessage: (channel) => {
    return channel.get(PUSH_QUEUE, { noAck: true });
  },
  sendMessage: (channel, msg) => {
    return channel.publish(WORKER_EXCHANGE, '', Buffer.from(msg));
  },
};

module.exports = service;
