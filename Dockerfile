FROM node:12-buster-slim

RUN mkdir /app
WORKDIR /app
COPY . .
RUN rm -rf .vscode .git .git .gitignore .sample-env \
     && npm install

RUN echo "#!/bin/bash" > run.sh && chmod +x run.sh
RUN echo "node index.js" >> run.sh
CMD ./run.sh
