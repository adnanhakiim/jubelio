require('dotenv').config();

const config = {
  NODE_ENV: process.env.NODE_ENV,
  RMQ_HOST: process.env.RMQ_HOST,
  RMQ_PORT: process.env.RMQ_PORT === undefined ? 30001 : parseInt(process.env.RMQ_PORT, 10),
  RMQ_USER: process.env.RMQ_USER,
  RMQ_PASSWORD: process.env.RMQ_PASSWORD,
  PUSH_QUEUE: process.env.PUSH_QUEUE || 'notify',
  ENABLE_LOG: false,
  ES_LOG_ID: process.env.ES_LOG_ID,
  ES_API_KEY: process.env.ES_API_KEY,
};

module.exports = config;
