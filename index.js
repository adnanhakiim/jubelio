const http = require('http');

const Logger = require('./logger');
const { openChannel, sendMessage } = require('./client');

const logger = new Logger('notify');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3030;

let rmqChannel;

function handleError(res, code) {
  const message = `{"error": "${http.STATUS_CODES[code]}"}`;
  logger.error(message);
  res.statusCode = code;
  res.end(message);
}

function handlePostReq(req, res) {
  const size = parseInt(req.headers['content-length'], 10);
  const buffer = Buffer.allocUnsafe(size);
  let pos = 0;

  if (req.url !== '/notify') {
    handleError(res, 404);
    return;
  }

  req
    .on('data', (chunk) => {
      const offset = pos + chunk.length;
      if (offset > size) {
        handleError(res, 413);
        return;
      }
      chunk.copy(buffer, pos);
      pos = offset;
    })
    .on('end', async () => {
      if (pos !== size) {
        handleError(res, 400);
        return;
      }

      try {
        if (rmqChannel) {
          sendMessage(rmqChannel, buffer.toString());
        }
      } catch (error) {
        logger.error(error.stack);
      } finally {
        // keep send ok status even on error
        res.setHeader('Content-Type', 'application/json;charset=utf-8');
        res.statusCode = 202;
        res.end('');
      }
    });
}

async function init() {
  const { channel } = await openChannel();
  rmqChannel = channel;
}

init().then(() => {
  const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
      return handlePostReq(req, res);
    }
    return handleError(res, 404);
  });

  server.listen(port, host, () => {
    logger.log(`Server listening on port ${port}`);
  });
});
