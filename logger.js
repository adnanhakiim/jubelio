const winston = require('winston');
const { ElasticsearchTransport } = require('winston-elasticsearch');

const config = require('./config');

let esTransportOpts;

let initialized = false;
let logger;
let esTransport;

if (config.ENABLE_LOG && config.ES_API_KEY && config.ES_LOG_ID) {
  esTransportOpts = {
    level: 'debug',
    retryLimit: 1,
    dataStream: true,
    clientOpts: {
      node: 'https://es.jubelio.com',
      auth: {
        apiKey: Buffer.from(`${config.ES_LOG_ID}:${config.ES_API_KEY}`).toString('base64'),
      },
    },
  };

  esTransport = new ElasticsearchTransport(esTransportOpts);

  logger = winston.createLogger({
    transports: [esTransport],
  });

  logger.on('error', (error) => {
    console.error('Error caught', error);
  });

  esTransport.on('warning', (error) => {
    console.error('Error caught', error);
  });

  initialized = true;
} else {
  logger = {
    log: (msg) => {
      console.log(msg);
    },
    error: (msg) => {
      console.error(msg);
    },
    info: (msg) => {
      console.info(msg);
    },
  };
}

class Logger {
  constructor(serviceName) {
    this.serviceName = serviceName;
    this.log(`Log initialized ${initialized}`);
  }

  log(message) {
    try {
      if (logger && config.ENABLE_LOG) {
        logger.info(message, this.createFields());
      } else {
        console.log(message);
      }
    } catch (err) {
      // do nothing
    }
  }

  error(message) {
    try {
      if (logger && config.ENABLE_LOG) {
        if (message && message.stack) {
          logger.error(message.stack, this.createFields());
        } else {
          logger.error(message, this.createFields());
        }
      }
    } catch (error) {
      // do nothing
    }
  }

  fatal(data, message) {
    try {
      if (logger && config.ENABLE_LOG) {
        const fields = this.createFields();
        logger.error(message, { fields, ...data });
      }
    } catch (error) {
      // do nothing
    }
  }

  createFields() {
    return {
      service: this.serviceName,
    };
  }

  // eslint-disable-next-line class-methods-use-this
  flush() {
    if (logger && esTransport) {
      return esTransport.flush();
    }
    return Promise.resolve();
  }
}

module.exports = Logger;
